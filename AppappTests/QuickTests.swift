import Quick
import Nimble
@testable import Appapp

class MenuViewControllerSpec: QuickSpec {

    override func spec() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController

        context("When the view controller is loaded") {
            //Trigger the view to load and assert that it's not nil
            expect(viewController.view).notTo(beNil())

            it("the texts are fetched from plist") {
                let items = viewController.items
                expect(items).notTo(beNil())
                expect(items.count).to(equal(4))

                let item = items[0]
                expect(item.text).to(contain("I am a developer"))
            }
        }
    }

}