import UIKit

struct MenuItem {
    let image: UIImage?
    let color: UIColor
    let title: String
    let text: String
}