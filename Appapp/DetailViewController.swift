//
// Created by Waldal, Marius on 21/03/16.
// Copyright (c) 2016 Lonely Penguin. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController : UIViewController, ZoomTransitioning {
    var selectedIndexPath: NSIndexPath?
    var item: MenuItem?

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!

    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeButtonTrailingConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let item = item {
            
            bgView.backgroundColor = item.color
            imageView.image = item.image

            titleLabel.text = item.title
            textView.text = item.text
        }
        else {
            bgView.backgroundColor = UIColor.grayColor()
            imageView.image = nil

            titleLabel.text = nil
            textView.text = nil
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.textView.setContentOffset(CGPointZero, animated: false)
    }
    
    @IBAction func handleClose(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

    func setupState(initial: Bool) {
        if initial {
            closeButtonTrailingConstraint.constant = 60
            titleLabelTopConstraint.constant = 1000
            imageView.alpha = 1
            bgView.alpha = 0
            textView.alpha = 0
            titleLabel.alpha = 0
        }
        else {
            closeButtonTrailingConstraint.constant = 0
            titleLabelTopConstraint.constant = 40
            imageView.alpha = 0.3
            bgView.alpha = 0
            textView.alpha = 1
            titleLabel.alpha = 1
        }
        view.layoutIfNeeded()
    }

    // MARK: ZoomTransitioning protocol

    func zoomTransitionBgViewForTransition(transition: ZoomTransition) -> UIView? {
        return bgView
    }

    func zoomTransitionImageViewForTransition(transition: ZoomTransition) -> UIImageView? {
        return imageView
    }

    func zoomTransition(transition: ZoomTransition,
                               willAnimateTransitionWithOperation operation: UINavigationControllerOperation,
                               isForegroundViewController isForeground: Bool) {

        setupState(operation == .Push)

        UIView.animateWithDuration(0.6, delay: operation == .Push ? 0.5 : 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: { () -> Void in
            [self]
            self.setupState(operation == .Pop)
        }) { (finished) -> Void in }
    }
}
