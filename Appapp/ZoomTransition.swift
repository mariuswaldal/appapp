import UIKit

@objc
protocol ZoomTransitioning {
    func zoomTransitionBgViewForTransition(transition: ZoomTransition) -> UIView?
    func zoomTransitionImageViewForTransition(transition: ZoomTransition) -> UIImageView?

    optional
    func zoomTransition(transition: ZoomTransition,
                        willAnimateTransitionWithOperation operation: UINavigationControllerOperation,
                        isForegroundViewController isForeground: Bool)
}

private let kZoomingIconTransitionDuration: NSTimeInterval = 0.6
private let kZoomingIconTransitionZoomedScale: CGFloat = 5
private let kZoomingIconTransitionBackgroundScale: CGFloat = 0.80

class ZoomTransition: NSObject, UIViewControllerAnimatedTransitioning, UINavigationControllerDelegate {
    var operation: UINavigationControllerOperation = .None

    enum TransitionState {
        case Initial
        case Final
    }

    typealias ZoomingViews = (bgView: UIView, imageView: UIView)

    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return kZoomingIconTransitionDuration
    }

    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let duration = transitionDuration(transitionContext)
        let containerView = transitionContext.containerView()

        let viewControllers = getViewControllers(transitionContext)
        let backgroundViewController = viewControllers.backgroundViewController
        let foregroundViewController = viewControllers.foregroundViewController

        let iconViews = getIconViews(backgroundViewController, foregroundViewController: foregroundViewController)
        let backgroundBgView = iconViews.backgroundBgView
        let foregroundBgView = iconViews.foregroundBgView
        let backgroundImageView = iconViews.backgroundImageView
        let foregroundImageView = iconViews.foregroundImageView

        containerView!.addSubview(backgroundViewController.view) // Must add to view hierarchy to snapshot
        let snapshots = createViewSnapshots(backgroundBgView, backgroundImageView: backgroundImageView)
        let snapshotOfBgView = snapshots.snapshotOfBgView
        let snapshotOfImageView = snapshots.snapshotOfImageView

        // Prepare animation
        backgroundBgView.hidden = true
        foregroundBgView.hidden = true
        backgroundImageView.hidden = true
        foregroundImageView.hidden = true
        
        containerView!.backgroundColor = UIColor(hexString: "4A90E2")
        containerView!.addSubview(backgroundViewController.view)
        containerView!.addSubview(snapshotOfBgView)
        containerView!.addSubview(foregroundViewController.view)
        containerView!.addSubview(snapshotOfImageView)
        
        let foregroundViewBackgroundColor = foregroundViewController.view.backgroundColor
        foregroundViewController.view.backgroundColor = UIColor.clearColor()

        var preTransitionState = TransitionState.Initial
        var postTransitionState = TransitionState.Final
        
        if operation == .Pop {
            preTransitionState = TransitionState.Final
            postTransitionState = TransitionState.Initial
        }
        
        configureViewsForState(preTransitionState, containerView: containerView!, backgroundViewController: backgroundViewController, viewsInBackground: (backgroundBgView, backgroundImageView), viewsInForeground: (foregroundBgView, foregroundImageView), snapshotViews: (snapshotOfBgView, snapshotOfImageView))
        
        // Perform animation
        (foregroundViewController as? ZoomTransitioning)?.zoomTransition?(self, willAnimateTransitionWithOperation: operation, isForegroundViewController: true)
        foregroundViewController.view.layoutIfNeeded()

        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: { () -> Void in
            [self]
            self.configureViewsForState(postTransitionState, containerView: containerView!, backgroundViewController: backgroundViewController, viewsInBackground: (backgroundBgView, backgroundImageView), viewsInForeground: (foregroundBgView, foregroundImageView), snapshotViews: (snapshotOfBgView, snapshotOfImageView))

        }, completion: {
            (finished) in

            backgroundViewController.view.transform = CGAffineTransformIdentity

            snapshotOfBgView.removeFromSuperview()
            snapshotOfImageView.removeFromSuperview()

            backgroundBgView.hidden = false
            foregroundBgView.hidden = true

            backgroundImageView.hidden = false
            foregroundImageView.hidden = false

            foregroundViewController.view.backgroundColor = backgroundBgView.backgroundColor//foregroundViewBackgroundColor

            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
    }

    func configureViewsForState(state: TransitionState, containerView: UIView, backgroundViewController: UIViewController, viewsInBackground: ZoomingViews, viewsInForeground: ZoomingViews, snapshotViews: ZoomingViews) {
        switch state {
        case .Initial:
            // Reset the transforms and make the background view visible (fades in by animation)
            backgroundViewController.view.transform = CGAffineTransformIdentity
            backgroundViewController.view.alpha = 1
            snapshotViews.bgView.transform = CGAffineTransformIdentity

            // Set the frame of the snapshot views to be the same as in the background (menu) view
            snapshotViews.bgView.frame = containerView.convertRect(viewsInBackground.bgView.frame, fromView: viewsInBackground.bgView.superview)
            snapshotViews.imageView.frame = containerView.convertRect(viewsInBackground.imageView.frame, fromView: viewsInBackground.imageView.superview)

        case .Final:
            // Shrink the background view slightly and make it invisible (fades out by animation)
            backgroundViewController.view.transform = CGAffineTransformMakeScale(kZoomingIconTransitionBackgroundScale, kZoomingIconTransitionBackgroundScale)
            backgroundViewController.view.alpha = 0

            // Enlarge the bgview to cover the entire screen and set its center so that it grows out of the center of the imageview
            snapshotViews.bgView.transform = CGAffineTransformMakeScale(kZoomingIconTransitionZoomedScale, kZoomingIconTransitionZoomedScale)
            snapshotViews.bgView.center = containerView.convertPoint(viewsInForeground.imageView.center, fromView: viewsInForeground.imageView.superview)
            // Set the imageview frame so that it matches the position it has in the foreground view controller
            snapshotViews.imageView.frame = containerView.convertRect(viewsInForeground.imageView.frame, fromView: viewsInForeground.imageView.superview)

        default:
            ()
        }
    }

    // MARK: UINavigationControllerDelegate

    func navigationController(navigationController: UINavigationController,
                              animationControllerForOperation operation: UINavigationControllerOperation,
                              fromViewController fromVC: UIViewController,
                              toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        if fromVC is ZoomTransitioning &&
                toVC is ZoomTransitioning {
            self.operation = operation
            return self
        }
        else {
            return nil
        }

    }

    // Mark: Helpers

    func getIconViews(backgroundViewController: UIViewController, foregroundViewController: UIViewController) ->
            (backgroundImageView: UIImageView,
             foregroundImageView: UIImageView,
             backgroundBgView: UIView,
             foregroundBgView: UIView) {

        let backgroundImageView = (backgroundViewController as? ZoomTransitioning)?.zoomTransitionImageViewForTransition(self)
        let foregroundImageView = (foregroundViewController as? ZoomTransitioning)?.zoomTransitionImageViewForTransition(self)
        assert(backgroundImageView != nil, "Cannot find image view in background view controller")
        assert(foregroundImageView != nil, "Cannot find image view in foreground view controller")

        let backgroundBgView = (backgroundViewController as? ZoomTransitioning)?.zoomTransitionBgViewForTransition(self)
        let foregroundBgView = (foregroundViewController as? ZoomTransitioning)?.zoomTransitionBgViewForTransition(self)
        assert(backgroundBgView != nil, "Cannot find background view in background view controller")
        assert(foregroundBgView != nil, "Cannot find background view in foreground view controller")

        return (backgroundImageView!, foregroundImageView!, backgroundBgView!, foregroundBgView!)
    }

    func getViewControllers(transitionContext: UIViewControllerContextTransitioning) ->
            (backgroundViewController: UIViewController, foregroundViewController: UIViewController) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        var backgroundViewController = fromViewController
        var foregroundViewController = toViewController

        if operation == .Pop {
            backgroundViewController = toViewController
            foregroundViewController = fromViewController
        }
        return (backgroundViewController, foregroundViewController)
    }

    func createViewSnapshots(backgroundBgView: UIView, backgroundImageView: UIImageView) ->
            (snapshotOfBgView: UIView, snapshotOfImageView: UIImageView) {

        let snapshotOfBgView = backgroundBgView.snapshotViewAfterScreenUpdates(false)
        let snapshotOfImageView = UIImageView(image: backgroundImageView.image)
        snapshotOfImageView.contentMode = .ScaleAspectFit
        return(snapshotOfBgView, snapshotOfImageView)
    }
    
}