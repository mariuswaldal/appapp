
import UIKit

private let reuseIdentifier = "Cell"

class MenuViewController: UICollectionViewController, ZoomTransitioning {
    let ApplicationKey = "Application"
    let AboutmeKey = "Aboutme"
    let ProjectsKey = "Projects"
    let ThisappKey = "Thisapp"
    var selectedIndexPath: NSIndexPath?
    var items : [MenuItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView!.contentInset = UIEdgeInsets(top: 100, left: 0, bottom: 0, right: 0)

        if let data = Data(name: "data") {
            //data.removePlistFile()
            let dict = data.getDataValues()!

            items.append(MenuItem(image: UIImage(named: "application"), color: UIColor(hexString: "d3eaf2")!, title: "Position as iOS Developer", text: dict[ApplicationKey] as! String))
            items.append(MenuItem(image: UIImage(named: "aboutme"), color: UIColor(hexString: "d3eaf2")!, title: "About me", text: dict[AboutmeKey] as! String))
            items.append(MenuItem(image: UIImage(named: "projects"), color: UIColor(hexString: "d3eaf2")!, title: "Projects", text: dict[ProjectsKey] as! String))
            items.append(MenuItem(image: UIImage(named: "imac"), color: UIColor(hexString: "d3eaf2")!, title: "About this app", text: dict[ThisappKey] as! String))
        } else {
            print("Unable to get Plist")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: UICollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 2
        default:
            return 0
        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MenuItemCell

        var index = 0
        for s in 0..<indexPath.section {
            index += self.collectionView(collectionView, numberOfItemsInSection: s)
        }
        index += indexPath.item

        let item = items[index]
        cell.item = item

        return cell
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidth = layout.itemSize.width
        
        let numberOfCells = self.collectionView(collectionView, numberOfItemsInSection: section)
        let widthOfCells = CGFloat(numberOfCells) * layout.itemSize.width + CGFloat(numberOfCells-1) * layout.minimumInteritemSpacing
        
        let inset = (collectionView.bounds.width - widthOfCells) / 2.0
        
        return UIEdgeInsets(top: 0, left: inset, bottom: 40, right: inset)
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        selectedIndexPath = indexPath

        var index = 0
        for s in 0..<indexPath.section {
            index += self.collectionView(collectionView, numberOfItemsInSection: s)
        }
        index += indexPath.item

        let item = items[index]
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
        controller.item = item

        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: ZoomTransitioning protocol

    func zoomTransitionBgViewForTransition(transition: ZoomTransition) -> UIView? {
        if let indexPath = selectedIndexPath {
            let cell = collectionView!.cellForItemAtIndexPath(indexPath) as! MenuItemCell
            return cell.bgView
        } else {
            return nil
        }
    }

    func zoomTransitionImageViewForTransition(transition: ZoomTransition) -> UIImageView? {
        if let indexPath = selectedIndexPath {
            let cell = collectionView!.cellForItemAtIndexPath(indexPath) as! MenuItemCell
            return cell.imageView
        } else {
            return nil
        }
    }

}
