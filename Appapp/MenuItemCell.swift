
import UIKit

class MenuItemCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageView: UIImageView!

    var item: MenuItem? {
        didSet {
            if let item = item {
                bgView.backgroundColor = item.color
                imageView.image = item.image
            }
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        bgView.layer.cornerRadius = bounds.width/2.0
        bgView.layer.masksToBounds = true
        imageView.layer.masksToBounds = true
    }
    
}
